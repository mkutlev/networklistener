package com.example.networklistener.devicelocation;

import android.graphics.PointF;

import com.example.networklistener.WifiItem;

public class WirelessRouter extends WifiItem {

    public static final int DEFAULT_SIGNAL_FLICKER = 2;
    
    // Range in meters
    private float mRange;

    /**
     * Signal level next to the router in dBm
     */
    private int mMaxSignalLevel;

    /**
     * Signal level change when the device is not moving
     */
    private int mSignalFlicker; // 2 -> + - 2 dBm

    /**
     * Position in meters
     */
    private PointF mPosition;

    public WirelessRouter(String name, String bssId, int signalLevel, int frequency, int maxSignalLevel, PointF position,
            int signalFlicker, float range) {
        super(name, NO_SIGNAL, frequency, bssId);
        mMaxSignalLevel = maxSignalLevel;
        mPosition = position;
        mSignalFlicker = signalFlicker;
        mRange = range;
    }
    
    public WirelessRouter(String name, String bssid, PointF position) {
          this(name, bssid, NO_SIGNAL, -1, NO_SIGNAL, position, DEFAULT_SIGNAL_FLICKER, -1);      
    }

    public float getRange() {
        return mRange;
    }

    public void setRange(float range) {
        this.mRange = range;
    }

    public int getMaxSignalLevel() {
        return mMaxSignalLevel;
    }

    public void setMaxSignalLevel(int maxSignalLevel) {
        this.mMaxSignalLevel = maxSignalLevel;
    }

    /**
     * Gets signal flicker which should be >= 0.
     * 
     * @return signal flicker or -1 of we have no information.
     */
    public int getSignalFlicker() {
        return mSignalFlicker;
    }

    /**
     * Sets signal flicker. It should be >= 0 or -1 value will be set.
     * 
     * @param signalFlicker
     */
    public void setSignalFlicker(int signalFlicker) {
        if (signalFlicker < 0) {
            this.mSignalFlicker = -1;
            return;
        }
        this.mSignalFlicker = signalFlicker;
    }

    public PointF getPosition() {
        return mPosition;
    }

    public void setPosition(PointF position) {
        this.mPosition = position;
    }

}
