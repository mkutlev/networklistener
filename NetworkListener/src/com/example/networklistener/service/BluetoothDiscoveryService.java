package com.example.networklistener.service;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.IBinder;

/**
 * Service class that starts Bluetooth discoveries over certain time interval.
 * 
 * @author Tsvetelin Harizanov
 */
public class BluetoothDiscoveryService extends Service {
    private BluetoothAdapter mBluetoothAdapter;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startBluetoothDiscovery();
        return START_NOT_STICKY;
    }

    @Override
    public void onLowMemory() {
        stopSelf();
    }

    @Override
    public void onDestroy() {
        stopBluetoothDiscovery();
    }

    /**
     * Enables Bluetooth on the current device and scans for discoverable Bluetooth devices.
     * 
     * <p>
     * NOTE: Cancelling a running discovery in order to start a new one actually prevents the app
     * from finding Bluetooth connections with weaker signal. Therefore, if the service attempts to
     * run a discovery and there's already one running, nothing should happen.
     * </p>
     */
    private void startBluetoothDiscovery() {
        if (mBluetoothAdapter.isDiscovering()) {
            return;
        }

        if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }

        mBluetoothAdapter.startDiscovery();
    }

    /**
     * If a discovery for Bluetooth devices is running, this method stops it.
     */
    private void stopBluetoothDiscovery() {
        if (mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.cancelDiscovery();
        }
    }

}
