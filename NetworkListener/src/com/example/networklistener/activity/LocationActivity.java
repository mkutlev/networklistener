package com.example.networklistener.activity;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.PointF;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.example.networklistener.DataController;
import com.example.networklistener.R;
import com.example.networklistener.WifiItem;
import com.example.networklistener.devicelocation.DeviceLocation;
import com.example.networklistener.devicelocation.WirelessRouter;
import com.example.networklistener.service.WifiScanService;
import com.example.networklistener.utils.LocationUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class LocationActivity extends Activity implements Observer {

    // Width in meters
    private float ROOM_WIDTH = 7.5f;

    // Height in meters
    private float ROOM_HEIGHT = 18.7f;

    private float mRoomWidthM;

    private float mRoomHeightM;

    private int mRoomWidthPx;

    private int mRoomHeightPx;

    private int mMaxRoomWidthPx;

    private int mMaxRoomHeightPx;

    private WirelessRouter mRouterIxmap;

    private WirelessRouter mRouterBM1;

    private WirelessRouter mRouterBM2;

    private DeviceLocation mDeviceLocation;

    private Point mLocationOnScreen;

    private View mLocationView;

    private RelativeLayout mRoomView;

    private TextView mCoordinatesView;

    private DataController mDataController;

    private AlarmManager mAlarmManager;

    private PendingIntent mWifiPendingIntent;

    private Intent mWifiIntent;

    private boolean mListenWifi;

    private int mWifiScanInterval;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        mRoomWidthM = ROOM_WIDTH;
        mRoomHeightM = ROOM_HEIGHT;

        mRoomView = (RelativeLayout)findViewById(R.id.room_layout);
        mLocationView = findViewById(R.id.location_view);
        mCoordinatesView = (TextView)findViewById(R.id.coordinates);
        mCoordinatesView.setText("Unknown location");

        mRouterIxmap = new WirelessRouter("Ixmap", "78:1d:ba:be:d7:af", new PointF(7.4f, 11.80f));
        mRouterBM1 = new WirelessRouter("BITMIXbg", "d4:ca:6d:27:29:67", new PointF(3.5f, 16.20f));
        mRouterBM2 = new WirelessRouter("BITMIXbg", "d4:ca:6d:27:29:5b", new PointF(1.5f, 9.0f));

        List<WirelessRouter> routers = new ArrayList<WirelessRouter>();
        routers.add(mRouterIxmap);
        routers.add(mRouterBM1);
        routers.add(mRouterBM2);

        mDeviceLocation = new DeviceLocation(this, mRoomWidthM, mRoomHeightM, routers);

        mDataController = DataController.getInstance(this);

        mRoomView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {

                    @Override
                    public void onGlobalLayout() {
                        // Should be called only once
                        mRoomView.getViewTreeObserver().removeGlobalOnLayoutListener(this);

                        mMaxRoomWidthPx = mRoomView.getMeasuredWidth();
                        mMaxRoomHeightPx = mRoomView.getMeasuredHeight();
                        startTrackingLocation();
                    }
                });

        mAlarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        retrieveUserPreferences();
        if (mListenWifi) {
            startWifiScanService();
        }
    }

    @Override
    protected void onPause() {
        mDeviceLocation.deleteObserver(this);
        stopWifiScanService();
        super.onPause();
    }

    private void setUpRoomView() {
        calcRoomSizesPx(mMaxRoomWidthPx, mMaxRoomHeightPx);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)mRoomView
                .getLayoutParams();
        params.width = mRoomWidthPx;
        params.height = mRoomHeightPx;
        mRoomView.setLayoutParams(params);
    }

    public void test(View v) {
        RelativeLayout roomView = (RelativeLayout)findViewById(R.id.room_layout);
        Log.e("TAG", roomView.toString());
    }

    @Override
    public void update(Observable observable, Object data) {
        if (isFinishing()) {
            return;
        }

        if (observable instanceof DeviceLocation) {
            Log.w("TAG", "DeviceLocation updated.");

            updateLocation();
        }

    }

    private void positionLocationView(int x, int y) {
        LayoutParams params = (LayoutParams)mLocationView.getLayoutParams();
        params.leftMargin = x;
        params.topMargin = y;
        mLocationView.setLayoutParams(params);
    }

    private void startTrackingLocation() {
        setUpRoomView();
        mDeviceLocation.addObserver(this);
        updateLocation();
    }

    /**
     * Calculate the sizes of the room in px preserving aspect ratio. This sizes are used to display
     * the room on the screen.
     * 
     * @param maxWidthPx the maximum size of room width.
     * @param maxHeightPx the maximum size of room height.
     * 
     */
    private void calcRoomSizesPx(int maxWidthPx, int maxHeightPx) {
        float roomRatio = mRoomWidthM / mRoomHeightM;

        if ((float)mMaxRoomWidthPx / mMaxRoomHeightPx >= roomRatio) {
            mRoomHeightPx = maxHeightPx;
            mRoomWidthPx = LocationUtil.metersToPx(mRoomWidthM, mRoomHeightPx / mRoomHeightM);
        } else {
            mRoomWidthPx = maxWidthPx;
            mRoomHeightPx = LocationUtil.metersToPx(mRoomHeightM, mRoomWidthPx / mRoomWidthM);
        }
    }

    private float getPxToMetersRatio() {
        return mRoomWidthPx / mRoomWidthM;
    }

    /**
     * Update the location of the device on the device.
     */
    private void updateLocation() {
        if (mDeviceLocation.getLocation() != null) {
            mLocationOnScreen = new Point(LocationUtil.metersToPx(mDeviceLocation.getLocation().x,
                    getPxToMetersRatio()), LocationUtil.metersToPx(mDeviceLocation.getLocation().y,
                    getPxToMetersRatio()));
            positionLocationView(mLocationOnScreen.x, mLocationOnScreen.y);

            mCoordinatesView.setText(String.format("(%.2f, %.2f)", mDeviceLocation.getLocation().x,
                    mDeviceLocation.getLocation().y));

            mLocationView.setVisibility(View.VISIBLE);
        } else {
            // If we don't know the position, we will hide the view
            mLocationView.setVisibility(View.GONE);

            mCoordinatesView.setText("Unknown location");
        }

    }

    /**
     * Starts a service which will run scans for nearby Wi-Fi access points over certain periods of
     * time.
     */
    private void startWifiScanService() {
        mWifiIntent = new Intent(this, WifiScanService.class);
        mWifiPendingIntent = PendingIntent.getService(this, 0, mWifiIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        mAlarmManager.setRepeating(AlarmManager.RTC, 0, mWifiScanInterval, mWifiPendingIntent);
    }

    /**
     * Stops the alarm and the service running the Wi-Fi scans.
     */
    private void stopWifiScanService() {
        if (mWifiPendingIntent != null) {
            mAlarmManager.cancel(mWifiPendingIntent);
            mWifiPendingIntent = null;
        }

        if (mWifiIntent != null) {
            stopService(mWifiIntent);
            mWifiIntent = null;
        }

        mDataController.clearItems(WifiItem.class);
    }

    /**
     * Retrieves all user preferences set from the preference screen.
     */
    private void retrieveUserPreferences() {
        Resources resources = getResources();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        mListenWifi = sharedPreferences.getBoolean(
                resources.getString(R.string.pref_checkbox_wifi), false);
        mWifiScanInterval = Integer.parseInt(sharedPreferences.getString(
                resources.getString(R.string.pref_millis_wifi), "20000"));
    }

}
