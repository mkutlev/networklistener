package com.example.networklistener.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.widget.ListView;
import android.widget.TextView;
import com.example.networklistener.R;
import java.util.Arrays;
import java.util.List;

/**
 * The Activity class for the preference screen.
 * 
 * @author Tsvetelin Harizanov
 */
public class PreferencesActivity extends PreferenceActivity {

    private Preference mBluetoothMillisPreference;

    private String[] mEntries;

    private List<String> mEntryValues;

    private Dialog mBluetoothAlertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        TextView alertDialogView = new TextView(this);
        alertDialogView.setText(R.string.bluetooth_alert_message);
        alertDialogView.setTextColor(Color.WHITE);
        mBluetoothAlertDialog = new AlertDialog.Builder(this).setView(alertDialogView)
                .setPositiveButton("OK", null).create();

        Resources resources = getResources();
        mEntries = resources.getStringArray(R.array.pref_list_entries);
        mEntryValues = Arrays.asList(resources.getStringArray(R.array.pref_list_entry_values));

        configurePreferenceScreenListView(resources);
        setListPreferencesSummaries(resources);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mBluetoothAlertDialog.isShowing()) {
            mBluetoothAlertDialog.dismiss();
        }
    }

    /**
     * Sets the developer generated relevant resources to this PreferenceActivity's ListView.
     * 
     * @param resources
     */
    private void configurePreferenceScreenListView(Resources resources) {
        ListView listView = getListView();

        int myPadding = (int)resources.getDimension(R.dimen.preference_activity_side_padding);
        listView.setPadding(myPadding, 0, myPadding, 0);

        listView.setBackgroundDrawable(resources
                .getDrawable(R.drawable.preference_screen_background));
        listView.setDivider(new ColorDrawable(resources
                .getColor(R.color.preference_listview_divider)));
        listView.setDividerHeight((int)resources.getDimension(R.dimen.preferences_divider_height));
    }

    /**
     * Gets the default shared preferences and sets the summaries for the ListPreference items. This
     * is necessary because summaries are not persistent.
     * 
     * @param resources
     */
    private void setListPreferencesSummaries(Resources resources) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        // adapting the Wi-Fi scans interval ListPreference
        String keyWifiMillis = resources.getString(R.string.pref_millis_wifi);
        Preference wifiMillisPreference = findPreference(keyWifiMillis);
        wifiMillisPreference
                .setSummary(parseString(sharedPreferences.getString(keyWifiMillis, "")));
        wifiMillisPreference.setOnPreferenceChangeListener(getOnPreferenceChangeListener());

        // adapting the Bluetooth discoveries interval ListPreference
        String keyBluetoothMillis = resources.getString(R.string.pref_millis_bluetooth);
        mBluetoothMillisPreference = findPreference(keyBluetoothMillis);
        mBluetoothMillisPreference.setSummary(parseString(sharedPreferences.getString(
                keyBluetoothMillis, "")));
        mBluetoothMillisPreference.setOnPreferenceChangeListener(getOnPreferenceChangeListener());
    }

    /**
     * Returns a listener to synchronize preference summary with preference value.
     * 
     * @return the listener
     */
    private Preference.OnPreferenceChangeListener getOnPreferenceChangeListener() {
        return new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(parseString(newValue));

                // 12000 milliseconds is the approximate time a single Bluetooth discovery lasts.
                // Warn the user that if the Bluetooth discovery interval is set to less than 12
                // seconds, say 10, the service will result in starting a discovery every 20 seconds
                // because when the service tries to run a discovery and detects that there's
                // already a discovery running, nothing will happen.
                if (preference.equals(mBluetoothMillisPreference)
                        && Integer.parseInt((String)newValue) < 12000) {
                    mBluetoothAlertDialog.show();
                }

                return true;
            }
        };
    }

    /**
     * Returns a human readable version of the preference value.
     * 
     * @param rawValue
     * @return readable value
     */
    private String parseString(Object rawValue) {
        return mEntries[mEntryValues.indexOf(rawValue)];
    }
}
