package com.example.networklistener;

/**
 * Class which describes a single ListView bluetooth item.
 * 
 * @author mkutlev
 * @since Aug 15, 2013
 */
public class BluetoothItem extends NetworkItem {

    private static final int sIconId = R.drawable.bluetooth_icon;

    public BluetoothItem(String name, int signalLevel) {
        super(name, signalLevel);
    }

    @Override
    public int getIconResourceId() {
        return sIconId;
    }

}
