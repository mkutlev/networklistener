package com.example.networklistener.utils;

/**
 * Util class for useful methods for finding location.
 * 
 * @author mkutlev
 * @since Aug 21, 2013
 */
public class LocationUtil {

    private LocationUtil() {

    }

    /**
     * Pixels to meters
     * 
     * @param pixels
     * @param metersToPxRatio
     * @return
     */
    public static float pxToMeters(int pixels, float metersToPxRatio) {

        if (metersToPxRatio <= 0) {
            return -1;
        }
        return metersToPxRatio * pixels;
    }

    /**
     * Meters to pixels
     * 
     * @param meters
     * @param pxToMetersRatio
     * @return
     */
    public static int metersToPx(float meters, float pxToMetersRatio) {

        if (pxToMetersRatio <= 0) {
            return -1;
        }
        return Math.round(pxToMetersRatio * meters);
    }

}
