package com.example.networklistener.devicelocation;

import android.content.Context;
import android.graphics.PointF;

import com.example.networklistener.NetworkItem;
import com.example.networklistener.DataController;
import com.example.networklistener.WifiItem;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Class which tracks device's location.
 * 
 * @author mkutlev
 * @since Aug 19, 2013
 */
public class DeviceLocation extends Observable implements Observer {

    private List<WirelessRouter> mRouters;

    private PointF mLocation;

    private float mRoomWidthM;

    private float mRoomHeightM;

    DataController mDataController;

    public DeviceLocation(Context context, float roomWidthM, float roomHeightM,
            List<WirelessRouter> routers) {
        mRoomWidthM = roomWidthM;
        mRoomHeightM = roomHeightM;
        mRouters = routers;

        mDataController = DataController.getInstance(context);
        mDataController.addObserver(this);
    }

    public List<WirelessRouter> getRouters() {
        return mRouters;
    }

    public void setRouters(List<WirelessRouter> routers) {
        mRouters = routers;
        updateLocation();
    }

    /**
     * Gets the physical location of the device in meters.
     * 
     * @return location as point
     */
    public PointF getLocation() {
        return mLocation;
    }

    private void updateLocation() {
        if (mRouters.size() >= 3) {
            mLocation = calculateLocation(mRouters.get(0), mRouters.get(1), mRouters.get(2));
        } else {
            mLocation = null;
        }
        setChanged();
        notifyObservers();
    }

    /**
     * Calculate the location of the device using the given parameters. They should be in meters.
     * 
     * @param router1
     * @param router2
     * @param router3
     * @return the location
     */
    private PointF calculateLocation(WirelessRouter router1, WirelessRouter router2,
            WirelessRouter router3) {

        // Cross points between circles formed from router1 and router2 - p1, p2
        PointF[] crossPointsR1R2 = getCrossPoints(router1, router2);
        // Cross points between circles formed from router1 and router3 - p3, p4
        PointF[] crossPointsR1R3 = getCrossPoints(router1, router3);

        if (crossPointsR1R2[0] != null && isInsideRoom(crossPointsR1R2[0])
                && crossPointsR1R2[1] != (null) && isInsideRoom(crossPointsR1R2[1])) {

            if (crossPointsR1R3[0] != null && isInsideRoom(crossPointsR1R3[0])
                    && crossPointsR1R3[1] != null && isInsideRoom(crossPointsR1R3[1])) {
                // We have 4 cross points inside the room.
                return getLocationPoint(crossPointsR1R2[0], crossPointsR1R2[1], crossPointsR1R3[0],
                        crossPointsR1R3[1]);
            } else if (crossPointsR1R3[0] != null && isInsideRoom(crossPointsR1R3[0])) {
                // We have 3 cross points inside the room.
                return getLocationPoint(crossPointsR1R2[0], crossPointsR1R2[1], crossPointsR1R3[0]);
            } else if (crossPointsR1R3[1] != null && isInsideRoom(crossPointsR1R3[1])) {
                // We have 3 cross points inside the room.
                return getLocationPoint(crossPointsR1R2[0], crossPointsR1R2[1], crossPointsR1R3[1]);
            } else {
                // We have only p1 and p2.
                return getMiddlePoint(crossPointsR1R2[0], crossPointsR1R2[1]);
            }
        } else if (crossPointsR1R2[0] != null && isInsideRoom(crossPointsR1R2[0])) {
            if (crossPointsR1R3[0] != null && isInsideRoom(crossPointsR1R3[0])
                    && crossPointsR1R3[1] != null && isInsideRoom(crossPointsR1R3[1])) {
                // We have 3 cross points inside the room.
                return getLocationPoint(crossPointsR1R3[0], crossPointsR1R3[1], crossPointsR1R2[0]);
            } else if (crossPointsR1R3[0] != null && isInsideRoom(crossPointsR1R3[0])) {
                // We have 2 cross points inside the room.
                return getMiddlePoint(crossPointsR1R2[0], crossPointsR1R3[0]);
            } else if (crossPointsR1R3[1] != null && isInsideRoom(crossPointsR1R3[1])) {
                // We have 2 cross points inside the room.
                return getMiddlePoint(crossPointsR1R2[0], crossPointsR1R3[1]);
            } else {
                // We have only p1.
                return crossPointsR1R2[0];
            }
        } else if (crossPointsR1R2[1] != null && isInsideRoom(crossPointsR1R2[1])) {
            if (crossPointsR1R3[0] != null && isInsideRoom(crossPointsR1R3[0])
                    && crossPointsR1R3[1] != null && isInsideRoom(crossPointsR1R3[1])) {
                // We have 3 cross points inside the room.
                return getLocationPoint(crossPointsR1R3[0], crossPointsR1R3[1], crossPointsR1R2[1]);
            } else if (crossPointsR1R3[0] != null && isInsideRoom(crossPointsR1R3[0])) {
                // We have 2 cross points inside the room.
                return getMiddlePoint(crossPointsR1R2[1], crossPointsR1R3[0]);
            } else if (crossPointsR1R3[1] != null && isInsideRoom(crossPointsR1R3[1])) {
                // We have 2 cross points inside the room.
                return getMiddlePoint(crossPointsR1R2[1], crossPointsR1R3[1]);
            } else {
                // We have only p2.
                return crossPointsR1R2[1];
            }
        } else {
            // p1 and p2 are null or outside the room.
            if (crossPointsR1R3[0] != null && isInsideRoom(crossPointsR1R3[0])
                    && crossPointsR1R3[1] != null && isInsideRoom(crossPointsR1R3[1])) {
                // p3 and p4 are inside the room.
                return getMiddlePoint(crossPointsR1R3[0], crossPointsR1R3[1]);
            } else if (crossPointsR1R3[0] != null && isInsideRoom(crossPointsR1R3[0])) {
                // We have only p3.
                return crossPointsR1R3[0];
            } else if (crossPointsR1R3[1] != null && isInsideRoom(crossPointsR1R3[1])) {
                // We have only p4.
                return crossPointsR1R3[1];
            } else {
                // None of the points is in the room.
                return null;
            }

        }

    }

    /**
     * Calculates the distance to the router using signal strength (dBm) and frequency.
     * 
     * @param dBm
     * @return the distance in meters
     */
    private double calculateDistance(int signalLevelInDb, int freqInMHz) {
        double exp = (27.55 - (20 * Math.log10(freqInMHz)) - signalLevelInDb) / 20.0;
        return Math.pow(10.0, exp);
    }

    private double calculateDistance(PointF point1, PointF point2) {
        // distance = Sqrt((x2-x1)^2 + (y2-y1)^2)
        return Math.sqrt(Math.pow(point2.x - point1.x, 2) + Math.pow(point2.y - point1.y, 2));
    }

    private boolean isInsideRoom(PointF point) {
        if ((point.x < 0) || (point.y < 0)) {
            return false;
        }

        return (point.x < mRoomWidthM) && (point.y < mRoomHeightM);
    }

    @Override
    public void update(Observable observable, Object data) {
        if (observable instanceof DataController) {
            updateRouters();
            updateLocation();
        }
    }

    private void updateRouters() {
        List<NetworkItem> items = mDataController.getNetworkItems();
        if (items == null || items.size() == 0) {
            return;
        }

        for (int i = 0; i < mRouters.size(); i++) {
            WirelessRouter router = mRouters.get(i);
            boolean updated = false;

            for (int j = 0; j < items.size(); j++) {
                if (items.get(j) instanceof WifiItem) {
                    WifiItem item = (WifiItem)items.get(j);
                    if (router.getName().equalsIgnoreCase(item.getName())
                            && router.getBssId().equalsIgnoreCase(item.getBssId())) {
                        router.setSignalLevel(item.getSignalLevel());
                        router.setFrequency(item.getFrequency());
                        updated = true;
                        break;
                    }
                }
            }

            if (!updated) {
                router.setSignalLevel(WifiItem.NO_SIGNAL);
                router.setFrequency(WifiItem.FREQUENCY_UNKNOWN);
            }

            sortRouters();
        }
    }

    private void sortRouters() {
        Collections.sort(mRouters, new Comparator<WirelessRouter>() {

            @Override
            public int compare(WirelessRouter object1, WirelessRouter object2) {
                if (object1.getName().equals(object2.getName())
                        && object1.getBssId().equals(object2.getBssId())) {
                    return 0;
                }

                if (object2.getSignalLevel() == WifiItem.NO_SIGNAL
                        || object2.getFrequency() == WifiItem.FREQUENCY_UNKNOWN) {
                    return -1;
                }

                if (calculateDistance(object1.getSignalLevel(), object1.getFrequency()) < calculateDistance(
                        object2.getSignalLevel(), object2.getFrequency())) {
                    return -1;
                } else {
                    return 1;
                }

            }
        });
    }

    private PointF[] getCrossPoints(WirelessRouter router1, WirelessRouter router2) {
        // Distance between router1 and the device (radius1)
        float r1 = (float)calculateDistance(router1.getSignalLevel(), router1.getFrequency());

        // Distance between router1 and the device (radius1)
        float r2 = (float)calculateDistance(router2.getSignalLevel(), router2.getFrequency());

        // The equation of the circle round router1:
        // (x - a)^2 + (y-b)^2 = r1^2
        float a = router1.getPosition().x;
        float b = router1.getPosition().y;

        // The equation of the circle round router2:
        // (x-c)^2 + (y-d)^2 = r2^2
        float c = router2.getPosition().x;
        float d = router2.getPosition().y;

        // x - y*(b-d)/(c-a) = eq1
        float eq1 = ((r1 * r1 - r2 * r2 - a * a - b * b + c * c + d * d) / (2 * (c - a)));

        // x = eq1 + y * eq2
        float eq2 = (b - d) / (c - a);

        // (eq2^2 + 1) y^2 + 2*(eq2 * (eq1-a) -b) * y + (eq1 - a)^2 + b^2 - r1^2 = 0
        float A = eq2 * eq2 + 1;
        float B = 2 * (eq2 * (eq1 - a) - b);
        float C = (eq1 - a) * (eq1 - a) + b * b - r1 * r1;

        PointF[] crossPoints = new PointF[2];

        if (A == 0) {
            // Linear equation
            float y = -C / B;
            float x = eq1 + y * eq2;

            crossPoints[0] = new PointF(x, y);
            return crossPoints;
        }

        // A * y^2 + 2 * B * y + C = 0
        // D = B^2 - 4 * A * C
        // Find D - discriminant
        float D = B * B - 4 * A * C;

        // The three cases depending on D
        if (D > 0) {
            float y1 = (-B + (float)Math.sqrt(D)) / (2 * A);
            float x1 = eq1 + y1 * eq2;
            crossPoints[0] = new PointF(x1, y1);

            float y2 = (-B - (float)Math.sqrt(D)) / (2 * A);
            float x2 = eq1 + y2 * eq2;
            crossPoints[1] = new PointF(x2, y2);
        } else if (D == 0) {
            float y = -B / (2 * A);
            float x = eq1 + y * eq2;

            crossPoints[0] = new PointF(x, y);
        } else {
            // D < 0 => the circles have no cross points
            // We will use the middle point between the circles
            crossPoints[0] = getMiddlePoint(router1, router2);
        }

        return crossPoints;
    }

    /**
     * Gets the middle point between two circles if they do not intersect.
     * 
     * @param router1
     * @param router2
     * @return the middle point or null if the circles do not intersect or we can not find the
     *         point.
     */
    private PointF getMiddlePoint(WirelessRouter router1, WirelessRouter router2) {
        if (router1.getSignalLevel() == WirelessRouter.NO_SIGNAL
                || router1.getFrequency() == WirelessRouter.FREQUENCY_UNKNOWN
                || router2.getSignalLevel() == WirelessRouter.NO_SIGNAL
                || router2.getFrequency() == WirelessRouter.FREQUENCY_UNKNOWN) {
            // Can not calculate r1 and r2.
            return null;
        }

        // Distance between router1 and the device (radius1)
        float r1 = (float)calculateDistance(router1.getSignalLevel(), router1.getFrequency());

        // Distance between router1 and the device (radius1)
        float r2 = (float)calculateDistance(router2.getSignalLevel(), router2.getFrequency());

        float r1r2 = (float)calculateDistance(router1.getPosition(), router2.getPosition());
        if (r1r2 < r1 + r2) {
            // The circles have intersection.
            return null;
        }

        // float radiusToAdd = (router1.getPosition().y < router2.getPosition().y) ? r1 : r2;
        float ratio = ((r1r2 - (r1 + r2)) / 2 + /* radiusToAdd */r1) / r1r2;

        float xLocation = router1.getPosition().x
                + (-router1.getPosition().x + router2.getPosition().x) * ratio;
        float yLocation = router1.getPosition().y
                + (-router1.getPosition().y + router2.getPosition().y) * ratio;

        return new PointF(xLocation, yLocation);
    }

    private PointF getMiddlePoint(PointF point1, PointF point2) {
        float xLocation = point1.x + (-point1.x + point2.x) * 2;
        float yLocation = point1.y + (-point1.y + point2.y) * 2;

        return new PointF(xLocation, yLocation);
    }

    /**
     * Calculate the location for given four points. These points has to be inside the room.
     * 
     * @param point1 first crossPoint of circles 1 and 2.
     * @param point2 second crossPoint of circles 1 and 2.
     * @param point3 first crossPoint of circles 1 and 3.
     * @param point4 second crossPoint of circles 1 and 3.
     * @return the location point
     */
    private PointF getLocationPoint(PointF point1, PointF point2, PointF point3, PointF point4) {
        double p1p3 = calculateDistance(point1, point3);
        double p1p4 = calculateDistance(point1, point4);

        double p2p3 = calculateDistance(point2, point3);
        double p2p4 = calculateDistance(point2, point4);

        PointF closeP1 = null;
        PointF closeP2 = null;

        if (p1p3 <= p1p4 && p1p3 <= p2p3 && p1p3 <= p2p4) {
            closeP1 = point1;
            closeP2 = point3;
        } else if (p1p4 < p2p3 && p1p4 < p2p4) {
            closeP1 = point1;
            closeP2 = point4;
        } else if (p2p3 < p2p4) {
            closeP1 = point2;
            closeP2 = point3;
        } else {
            // p2 and p4 are the closest points
            closeP1 = point2;
            closeP2 = point4;
        }

        return getMiddlePoint(closeP1, closeP2);
    }

    /**
     * Calculate the location for given three points. These points has to be inside the room.
     * 
     * @param point1 first crossPoint of circles 1 and 2.
     * @param point2 second crossPoint of circles 1 and 2.
     * @param point3 crossPoint of circles 1 and 3.
     * 
     * @return the location point
     */
    private PointF getLocationPoint(PointF point1, PointF point2, PointF point3) {
        double p1p3 = calculateDistance(point1, point3);
        double p2p3 = calculateDistance(point2, point3);

        PointF closeP1 = null;
        PointF closeP2 = null;

        if (p1p3 < p2p3) {
            closeP1 = point1;
            closeP2 = point3;
        } else {
            closeP1 = point1;
            closeP2 = point3;
        }

        return getMiddlePoint(closeP1, closeP2);
    }

}
