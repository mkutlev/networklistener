package com.example.networklistener.activity;

import android.app.AlarmManager;
import android.app.ListActivity;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.networklistener.BluetoothItem;
import com.example.networklistener.DataController;
import com.example.networklistener.R;
import com.example.networklistener.WifiItem;
import com.example.networklistener.adapter.ListViewAdapter;
import com.example.networklistener.receiver.BluetoothEventReceiver;
import com.example.networklistener.service.BluetoothDiscoveryService;
import com.example.networklistener.service.WifiScanService;

/**
 * The main Activity class for this application.
 * 
 * @author Tsvetelin Harizanov
 */
public class MainActivity extends ListActivity {
    private ListViewAdapter mAdapter;

    private DataController mDataController;

    private BluetoothEventReceiver mBluetoothEventReceiver;

    private AlarmManager mAlarmManager;

    private PendingIntent mWifiPendingIntent, mBluetoothPendingIntent;

    private Intent mWifiIntent, mBluetoothIntent;

    private boolean mListenWifi, mListenBluetooth;

    private int mWifiScanInterval, mBluetooothDiscoveryInterval;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAlarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        mDataController = DataController.getInstance(this);
        mAdapter = ListViewAdapter.getInstance(this);
        setListAdapter(mAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        retrieveUserPreferences();
        if (mListenWifi)
            startWifiScanService();

        if (mListenBluetooth)
            startBluetoothDiscoveryService();
        else if (!mListenWifi) {
            Toast.makeText(this, "Not listening to Wi-Fi nor Bluetooth.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopWifiScanService();
        stopBluetoothDiscoveryService();
        mAdapter.clear();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_settings:
                startActivity(new Intent(this, PreferencesActivity.class));
                return true;
            case R.id.menu_location:
                startActivity(new Intent(this, LocationActivity.class));
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Retrieves all user preferences set from the preference screen.
     */
    private void retrieveUserPreferences() {
        Resources resources = getResources();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        mListenWifi = sharedPreferences.getBoolean(
                resources.getString(R.string.pref_checkbox_wifi), false);
        mListenBluetooth = sharedPreferences.getBoolean(
                resources.getString(R.string.pref_checkbox_bluetooth), false);
        mWifiScanInterval = Integer.parseInt(sharedPreferences.getString(
                resources.getString(R.string.pref_millis_wifi), "20000"));
        mBluetooothDiscoveryInterval = Integer.parseInt(sharedPreferences.getString(
                resources.getString(R.string.pref_millis_bluetooth), "20000"));
    }

    /**
     * Registers a receiver to catch discovered devices and starts a service which will run
     * Bluetooth discoveries over certain periods of time.
     */
    private void startBluetoothDiscoveryService() {
        // register the BroadcastReceiver
        mBluetoothEventReceiver = new BluetoothEventReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothDevice.ACTION_FOUND);
        intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        registerReceiver(mBluetoothEventReceiver, intentFilter);

        // start the BluetoothDiscoveryService
        mBluetoothIntent = new Intent(this, BluetoothDiscoveryService.class);
        mBluetoothPendingIntent = PendingIntent.getService(this, 0, mBluetoothIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        mAlarmManager.setRepeating(AlarmManager.RTC, 0, mBluetooothDiscoveryInterval,
                mBluetoothPendingIntent);
    }

    /**
     * Starts a service which will run scans for nearby Wi-Fi access points over certain periods of
     * time.
     */
    private void startWifiScanService() {
        mWifiIntent = new Intent(this, WifiScanService.class);
        mWifiPendingIntent = PendingIntent.getService(this, 0, mWifiIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        mAlarmManager.setRepeating(AlarmManager.RTC, 0, mWifiScanInterval, mWifiPendingIntent);
    }

    /**
     * Unregisters the receiver catching the discovered devices and stops the alarm and the service
     * running the Bluetooth discoveries.
     */
    private void stopBluetoothDiscoveryService() {
        if (mBluetoothEventReceiver != null) {
            unregisterReceiver(mBluetoothEventReceiver);
            mBluetoothEventReceiver = null;
        }

        if (mBluetoothPendingIntent != null) {
            mAlarmManager.cancel(mBluetoothPendingIntent);
            mBluetoothPendingIntent = null;
        }

        if (mBluetoothIntent != null) {
            stopService(mBluetoothIntent);
            mBluetoothIntent = null;
        }

        mDataController.clearItems(BluetoothItem.class);
    }

    /**
     * Stops the alarm and the service running the Wi-Fi scans.
     */
    private void stopWifiScanService() {
        if (mWifiPendingIntent != null) {
            mAlarmManager.cancel(mWifiPendingIntent);
            mWifiPendingIntent = null;
        }

        if (mWifiIntent != null) {
            stopService(mWifiIntent);
            mWifiIntent = null;
        }

        mDataController.clearItems(WifiItem.class);
    }

}
