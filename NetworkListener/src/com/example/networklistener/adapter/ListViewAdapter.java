package com.example.networklistener.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.networklistener.NetworkItem;
import com.example.networklistener.R;
import java.util.ArrayList;
import java.util.List;

/**
 * The adapter class for the MainActivity's ListView.
 * 
 * @author Tsvetelin Harizanov
 */
public class ListViewAdapter extends BaseAdapter {

    private static ListViewAdapter sInstance;

    private Resources mResources;

    private LayoutInflater mInflater;

    private List<NetworkItem> mNetworkItems;

    private ListViewAdapter(Context context) {
        mResources = context.getResources();
        mInflater = LayoutInflater.from(context);
        mNetworkItems = new ArrayList<NetworkItem>();
    }

    /**
     * Always returns the same instance of this class.
     * 
     * @param context
     * @return the singleton instance
     */
    public static ListViewAdapter getInstance(Context context) {
        if (sInstance == null)
            sInstance = new ListViewAdapter(context);
        return sInstance;
    }

    @Override
    public int getCount() {
        return mNetworkItems.size();
    }

    @Override
    public NetworkItem getItem(int position) {
        try {
            return mNetworkItems.get(position);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;

        if (row == null) {
            row = mInflater.inflate(R.layout.list_item, parent, false);
            holder = new ViewHolder();
            holder.imageView = (ImageView)row.findViewById(R.id.imageview);
            holder.nameView = (TextView)row.findViewById(R.id.textview_name);
            holder.signalView = (TextView)row.findViewById(R.id.textview_signal);
            row.setTag(holder);
        } else {
            holder = (ViewHolder)row.getTag();
        }

        // apply all item attributes
        final NetworkItem item = getItem(position);
        holder.imageView.setImageDrawable(mResources.getDrawable(item.getIconResourceId()));
        holder.nameView.setText(item.getName());
        holder.signalView.setText(item.getSignalLevel() + " dBm");

        return row;
    }

    @Override
    public boolean isEmpty() {
        return mNetworkItems.isEmpty();
    }

    /**
     * Appends a new list of items to the existing items.
     * 
     * @param items - the new list of items
     */
    public void addItems(List<NetworkItem> items) {
        if (items == null || items.isEmpty())
            return;
        mNetworkItems.addAll(items);
        notifyDataSetChanged();
    }

    /**
     * Removes all items from this adapter.
     */
    public void clear() {
        if (!isEmpty())
            mNetworkItems.clear();
    }

    /**
     * Replaces the existing items with a new list of items.
     * 
     * @param items - the new list of items
     */
    public void setItems(List<NetworkItem> items) {
        clear();
        addItems(items);
    }

    /**
     * Class that describes a single adapter item.
     * 
     * @author Tsvetelin Harizanov
     */
    private static class ViewHolder {
        ImageView imageView;

        TextView nameView, signalView;
    }

}
