package com.example.networklistener.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.util.Log;

import com.example.networklistener.DataController;
import com.example.networklistener.WifiItem;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Service class that scans for Wi-Fi access points over certain time interval.
 * 
 * @author Tsvetelin Harizanov
 */
public class WifiScanService extends Service {
    private DataController mDataController;

    private WifiManager mWifiManager;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        mDataController = DataController.getInstance(this);
        mWifiManager = (WifiManager)getSystemService(Context.WIFI_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        retrieveWifiAccessPoints();
        return START_NOT_STICKY;
    }

    @Override
    public void onLowMemory() {
        stopSelf();
    }

    /**
     * Enables Wi-Fi on the current device, runs a scan for Wi-Fi access points, passes the
     * discovered ones to the DataController and updates the adapter.
     */
    private void retrieveWifiAccessPoints() {
        if (!mWifiManager.isWifiEnabled()) {
            mWifiManager.setWifiEnabled(true);
        }

        if (!mWifiManager.startScan()) {
            return;
        }

        mDataController.clearItems(WifiItem.class);

        List<ScanResult> accessPoints = mWifiManager.getScanResults();

        if (accessPoints == null || accessPoints.isEmpty()) {
            return;
        }

        for (ScanResult wifi : accessPoints) {
            DecimalFormat format = new DecimalFormat("#.##");
            
            mDataController.addNetworkItem(new WifiItem(wifi.SSID, wifi.level, wifi.frequency,
                    wifi.BSSID));
            
            Log.w("TAG WIFI",
                    "SSID: " + wifi.SSID + ", BSSID: " + wifi.BSSID + ", SignalLevel: "
                            + wifi.level + "dBm, distance: "
                            + format.format(calculateDistance((double)wifi.level, wifi.frequency))
                            + "m");
        }

        mDataController.updateAdapter();
    }

    /**
     * Used for logs.
     */
    private double calculateDistance(double signalLevelInDb, double freqInMHz) {
        double exp = (27.55 - (20 * Math.log10(freqInMHz)) - signalLevelInDb) / 20.0;
        return Math.pow(10.0, exp);
    }

}
