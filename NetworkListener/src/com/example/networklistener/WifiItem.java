package com.example.networklistener;

/**
 * Class which describes a single Wifi item.
 * 
 * @author mkutlev
 * @since Aug 15, 2013
 */
public class WifiItem extends NetworkItem {

    public static final int FREQUENCY_UNKNOWN = -1;
    
    private static final int sIconId = R.drawable.wifi_icon;

    private int mFrequency;

    private String mBssId;

    public WifiItem(String name, int signalLevel, int frequency, String bssId) {
        super(name, signalLevel);
        this.mFrequency = frequency;
        this.mBssId = bssId;
    }

    public WifiItem(String name, int signalLevel) {
        this(name, signalLevel, -1, null);
    }

    @Override
    public int getIconResourceId() {
        return sIconId;
    }

    /**
     * Gets signal's frequency.
     * 
     * @return -1 if we don't know it.
     */
    public int getFrequency() {
        return mFrequency;
    }

    public void setFrequency(int frequency) {
        this.mFrequency = frequency;
    }

    public String getBssId() {
        return mBssId;
    }

    public void setBSSID(String bSSID) {
        mBssId = bSSID;
    }

}
