package com.example.networklistener;

import android.content.Context;

import com.example.networklistener.adapter.ListViewAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Observable;

/**
 * Class that holds and sorts all incoming items and updates the ListView.
 * 
 * @author Tsvetelin Harizanov
 */
public class DataController extends Observable {
    private static DataController sSingletonInstance;

    private ListViewAdapter mAdapter;

    private List<NetworkItem> mNetworkItems;

    private DataController(Context context) {
        mAdapter = ListViewAdapter.getInstance(context);
        mNetworkItems = new ArrayList<NetworkItem>();
    }

    /**
     * Always returns the same instance of this class.
     * 
     * @param context
     * @return the singleton instance
     */
    public static DataController getInstance(Context context) {
        if (sSingletonInstance == null)
            sSingletonInstance = new DataController(context);
        return sSingletonInstance;
    }

    public List<NetworkItem> getNetworkItems() {
        return mNetworkItems;
    }
    
    /**
     * Adds a new network item.
     * 
     * @param item
     */
    public void addNetworkItem(NetworkItem item) {
        if (item != null) {
            mNetworkItems.add(item);
        }
    }

    /**
     * Removes all network items.
     */
    public void clearAllItems() {
        mNetworkItems.clear();
    }

    /**
     * Removes all network items which are instances of the provided class.
     * 
     * @param clazz class of the items to be removed
     * @see #clearAllItems()
     */
    public void clearItems(Class<? extends NetworkItem> clazz) {
        List<NetworkItem> itemsToRemove = new ArrayList<NetworkItem>();
        for (NetworkItem item : mNetworkItems) {
            if (clazz.isInstance(item)) {
                itemsToRemove.add(item);
            }
        }

        mNetworkItems.removeAll(itemsToRemove);
    }

    /**
     * Sorts the items by signal level and refresh adapter's items.
     */
    public void updateAdapter() {
        if (!mNetworkItems.isEmpty()) {
            sortBySignalLevel(mNetworkItems);
            mAdapter.setItems(mNetworkItems);
        }
        
        setChanged();
        notifyObservers();
    }

    /**
     * Sorts the items of the specified list by their signal level attribute.
     * 
     * @param items
     */
    private void sortBySignalLevel(List<NetworkItem> items) {
        Collections.sort(items, new Comparator<NetworkItem>() {

            @Override
            public int compare(NetworkItem item1, NetworkItem item2) {
                int current = item1.getSignalLevel();
                int next = item2.getSignalLevel();

                if (current < next)
                    return 1;
                else if (current == next)
                    return 0;
                else
                    return -1;
            }
        });
    }

}
