package com.example.networklistener;


/**
 * Abstract class that describes a single ListView network item.
 * 
 * @author mkutlev
 * @since Aug 15, 2013
 */
public abstract class NetworkItem {

    public static final int NO_SIGNAL = Integer.MIN_VALUE;

    private String mName;

    private int mSignalLevel;

    public NetworkItem(String name, int signalLevel) {
        this.mName = name;
        this.mSignalLevel = signalLevel;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    /**
     * Gets signal level. If it is equal to {@link NetworkItem#NO_SIGNAL} so there is no signal.
     */
    public int getSignalLevel() {
        return mSignalLevel;
    }

    /**
     * Sets signal level. Use {@link NetworkItem#NO_SIGNAL} if there is no signal.
     */
    public void setSignalLevel(int signalLevel) {
        this.mSignalLevel = signalLevel;
    }

    /**
     * Gets resource id pointing network's icon.
     * 
     * @return icon's id or {@code 0} if this network item has no icon
     */
    public abstract int getIconResourceId();

}
