package com.example.networklistener.receiver;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.example.networklistener.BluetoothItem;
import com.example.networklistener.DataController;

/**
 * BroadcastReceiver class meant to listen for Bluetooth actions.
 * 
 * @author Tsvetelin Harizanov
 */
public class BluetoothEventReceiver extends BroadcastReceiver {
    private static final String STARTED = BluetoothAdapter.ACTION_DISCOVERY_STARTED;

    private static final String FOUND = BluetoothDevice.ACTION_FOUND;

    private DataController mDataController;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (mDataController == null) {
            mDataController = DataController.getInstance(context);
        }

        if (intent.getAction().equalsIgnoreCase(FOUND)) {
            addDiscoveredDevice(intent);
        } else if (intent.getAction().equalsIgnoreCase(STARTED)) {
            mDataController.clearItems(BluetoothItem.class);
        }
    }

    /**
     * Gets the name and RSSI of the discovered Bluetooth device, includes them into a new item,
     * adds the item to the list of Bluetooth items and updates the adapter.
     * 
     * @param intent - the intent received from the BroadcastReceiver's onReceive() method
     */
    private void addDiscoveredDevice(Intent intent) {
        String name = intent.getStringExtra(BluetoothDevice.EXTRA_NAME);
        short rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, Short.MIN_VALUE);
        Log.e("TAG - Bluetooth", "Intent: " + intent + "\nExtras: " + intent.getExtras());
        mDataController.addNetworkItem(new BluetoothItem(name, rssi));
        mDataController.updateAdapter();
    }

}
